jQuery(document).ready(function($) {
    var navHolder = $('.nav-holder'),
        burgerMenu = navHolder.find('.hamburger'),
        closeBurger = navHolder.find('.close');
    burgerMenu.on('click', function() {
        navHolder.addClass('open-nav');
    });
    closeBurger.on('click', function() {
        navHolder.removeClass('open-nav');
    });
    var lastScrollTop = 0;
    //Scroll functionality
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop(),
            windowHeight = $(window).height(),
            windowWidth = $(window).width(),
            coolScroll = $('.section-cool-filler').offset(),
            portfolioTitle = $('.section-portfolio .portfolio-title h2'),
            portfolioTitleOffset = portfolioTitle.offset(),
            portfolioTitleSpan = portfolioTitle.find('span'),
            leftItem = $('.section-portfolio .left-portfolio-side'),
            rightItem = $('.section-portfolio .right-portfolio-side'),
            portfoliotContainer = $('.section-portfolio .container');
            portfolioOffset = portfoliotContainer.offset(),
            portfolioGridItem = portfoliotContainer.find('.vc_grid-item'),
            specialTitleKing = $('.special-title.king');
            kingOffset = specialTitleKing.offset(),
            planeHolder = $('.plane-holder'),
            planeHeight = planeHolder.height(),
            sectionContact = $('.section-contact'),
            sectionVideo = $('.section-video'),
            contactOffset = sectionContact.offset(),
            videoOffset = sectionVideo.offset(),
            scaleCrown = $('.scale-crown'),
            crownHeight = scaleCrown.height();
        if(scroll >= (coolScroll.top - windowHeight) - 50 ) {
            $('.section-cool-filler h2:nth-child(2)').css('left', '' + scroll / 100 + '%');
            $('.section-cool-filler h2:nth-child(4)').css('left', '' + ((scroll / 100) - 5) + '%');
            $('.section-cool-filler h2:nth-child(6)').css('left', '' + ((scroll / 100) - 10) + '%');
            $('.section-cool-filler h2:nth-child(1)').css('left', '-' + ((scroll / 100) - 27) + '%');
            $('.section-cool-filler h2:nth-child(3)').css('left', '-' + ((scroll / 100) - 30) + '%');
            $('.section-cool-filler h2:nth-child(5)').css('left', '-' + ((scroll / 100) - 32) + '%');
            $('.section-cool-filler h2:nth-child(7)').css('left', '-' + ((scroll / 100) - 35) + '%');
        }
        if(scroll >= kingOffset.top - windowHeight) {
            specialTitleKing.find('h2 span img').addClass('show-crown');
        } else {
            specialTitleKing.find('h2 span img').removeClass('show-crown');
        }
        $('.scratch-title h2 span').each(function() {
            var h2Span = $(this),
                h2SpanTop = h2Span.offset().top;
                if(scroll >= (h2SpanTop - windowHeight) + 300) {
                    h2Span.addClass('swipe-in');
                } else {
                    h2Span.removeClass('swipe-in');
                }
        });
        portfolioGridItem.each(function() {
            var thisGridItem = $(this),
                thisGridTop = thisGridItem.offset().top;
                if(scroll >= (thisGridTop - windowHeight) - 50) {
                    thisGridItem.addClass('show-grid-item');
                } else {
                    thisGridItem.removeClass('show-grid-item');
                }
        });
        portfolioTitleSpan.each(function() {
            var thisSpan = $(this),
                spanTop = thisSpan.offset().top;
                if(scroll >= spanTop - windowHeight) {
                    thisSpan.addClass('show');
                } else {
                    thisSpan.removeClass('show');
                }
        });
        if(scroll >= contactOffset.top - windowHeight) {
            var startPoint = contactOffset.top - windowHeight,
                endPoint = contactOffset.top + planeHeight,
                theDevider = (endPoint - startPoint) / 100,
                leftPosition = (scroll - startPoint) / theDevider; 
            planeHolder.css('left', '' + leftPosition + '%'); 
            var st = $(this).scrollTop();
            if (st > lastScrollTop){
                planeHolder.removeClass('turn');
            } else {
                planeHolder.addClass('turn');
            }

            lastScrollTop = st;
        }
        if(scroll >= videoOffset.top - windowHeight) {
            var startPoint = (videoOffset.top + videoOffset.top) + windowHeight,
                endPoint = videoOffset.top - windowHeight,
                theDevider = (endPoint - startPoint) / 100,
                scaleAmmount = (((scroll * 2.25) - startPoint) / theDevider) / 50; 
                scaleCrown.css('transform', 'rotate(24deg) scale(' + scaleAmmount + ', ' + scaleAmmount + ')'); 
        }
        leftItem.css('margin-top', '-' + ((scroll / 5) - 75) + 'px');
        rightItem.css('margin-top', '' + ((scroll / 12.5) - 75) + 'px');        
    });

    //Slick sliders
    $('.section-cta  > div > .vc_column-inner > .wpb_wrapper').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false
    });
});

  